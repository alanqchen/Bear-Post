const theme = {
    backgroundDark: "#0e0e10 !important",
    backgroundDarkAlt: "#212426 !important",
    backgroundDarkLight: "rgb(63, 64, 68) !important"
};

export default theme;