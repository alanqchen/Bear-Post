module github.com/alanqchen/Bear-Post/backend

go 1.13

require (
	github.com/SlyMarbo/gmail v0.0.0-20170317233059-3ec3141ee13b
	github.com/alanqchen/MGBlog/backend v0.0.0-20200211215356-bd380ca45d01 // indirect
	github.com/dchest/passwordreset v0.0.0-20190826080013-4518b1f41006
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/go-redis/redis/v7 v7.0.0-beta.5
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/jackc/pgtype v1.1.0
	github.com/jackc/pgx v3.6.1+incompatible
	github.com/jackc/pgx/v4 v4.3.0
	github.com/mholt/archiver v3.1.1+incompatible // indirect
	github.com/nickalie/go-binwrapper v0.0.0-20190114141239-525121d43c84 // indirect
	github.com/nickalie/go-webpbin v0.0.0-20170427122138-7e79cf5bb01e
	github.com/nwaples/rardecode v1.1.0 // indirect
	github.com/pierrec/lz4 v2.5.2+incompatible // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be
	github.com/satori/go.uuid v1.2.0
	github.com/ulikunitz/xz v0.5.7 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	github.com/yusukebe/go-pngquant v0.0.0-20200223090257-49b91f11b627
	go.mongodb.org/mongo-driver v1.2.1
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
)
